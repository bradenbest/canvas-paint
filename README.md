Canvas Paint is a simple program with some pretty cool optimizations.

It packs a `Uint8Array` buffer where each number represents a color, making it possible to create a layer for the
drawing.

The canvas shows 3 layers:
* background
* drawing
* mouse (hidden when not hovering over canvas)

Drawing this pixel by pixel with `ctx.fillRect` is terribly slow, so instead, I use `ctx.createImageData`, which is
faster by an *insane* margin.

Finally, regarding the mousemove event and it firing too slow to get smooth drawing, I implemented some simple linear
interpolation to fill in the gaps between each mouse position poll, which means no matter how fast you move the mouse,
the drawing is smooth.

![img](coffeecup.png)
