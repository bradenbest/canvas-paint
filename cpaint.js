const canvas = document.querySelector(".canvas");
const ctx = canvas.getContext("2d");

const mouse = Object.seal({
    x: 0,
    y: 0,
    lastx: 0,
    lasty: 0,
    down: 0,
    interp: []
});

const
    CL_BLACK    = 0,
    CL_WHITE    = 1,
    CL_RED      = 2,
    CL_GREEN    = 3,
    CL_BLUE     = 4,
    CL_YELLOW   = 5,
    CL_CYAN     = 6,
    CL_MAGENTA  = 7,
    CL_SRED     = 8,
    CL_SGREEN   = 9,
    CL_SBLUE    = 10,
    CL_SYELLOW  = 11,
    CL_SCYAN    = 12,
    CL_SMAGENTA = 13,
    CL_ORANGE   = 14,
    CL_BROWN    = 15,
    CL_PURPLE   = 16,
    CL_DGRAY    = 17,
    CL_MGRAY    = 18,
    CL_LGRAY    = 19,
    CL_DRED     = 20,
    CL_DGREEN   = 21,
    CL_DBLUE    = 22,
    CL_CUSTOM = 23,
    CL_END    = 24;
// can go up to 255

const colornames = [
    "Black",
    "White",
    "Red",
    "Green",
    "Blue",
    "Yellow",
    "Cyan",
    "Magenta",
    "Soft Red",
    "Soft Green",
    "Soft Blue",
    "Soft Yellow",
    "Soft Cyan",
    "Soft Magenta",
    "Orange",
    "Brown",
    "Purple",
    "Dark Gray",
    "Medium Gray",
    "Light Gray",
    "Dark Red",
    "Dark Green",
    "Dark Blue",
    "Custom",
];

const colors = [
    0x000000,
    0xffffff,
    0xff0000,
    0x00ff00,
    0x0000ff,
    0xffff00,
    0x00ffff,
    0xff00ff,
    0xff7777,
    0x77ff77,
    0x7777ff,
    0xffff77,
    0x77ffff,
    0xff77ff,
    0xff7700,
    0x773300,
    0x7700ff,
    0x333333,
    0x777777,
    0xcccccc,
    0x770000,
    0x007700,
    0x000077,
    0x000000
];

let current_color = CL_WHITE;
let imgdata;
let imgdata_cur_color = CL_BLACK;
let canvas_size = 500;
let brush_size = 15;
let canvasbuf;
let show_cursor = 1;

function resize_canvas(newsz){
    canvas_size = newsz;
    canvasbuf = new Uint8Array(canvas_size * canvas_size);
    canvas.width = newsz;
    canvas.height = newsz;
}

function resize_brush(newsz){
    brush_size = newsz;
}

function imgdata_set_color(color){
    imgdata_cur_color = color;
}

function imgdata_set_pixels(x, y, w, h){
    let selcolor = (get_palette_color(imgdata_cur_color))|0;
    let red   = (selcolor & 0xff0000) >> 16;
    let green = (selcolor & 0x00ff00) >> 8;
    let blue  = (selcolor & 0x0000ff);
    let idx;

    for(let iy = y; iy < y + h; ++iy){
        for(let ix = x; ix < x + w; ++ix){
            idx = (iy * canvas_size + ix) * 4;
            imgdata.data[idx + 0] = red;
            imgdata.data[idx + 1] = green;
            imgdata.data[idx + 2] = blue;
            imgdata.data[idx + 3] = 0xff;
        }
    }
}

function imgdata_draw(){
    ctx.putImageData(imgdata, 0, 0);
}

function get_palette_color(idx){
    if(idx < CL_END && idx >= 0)
        return colors[idx];

    return colors[CL_BLACK];
}

function draw_buffer(){
    let idx;

    for(let y = 0; y < canvas_size; ++y){
        for(let x = 0; x < canvas_size; ++x){
            idx = y * canvas_size + x;
            imgdata_set_color(canvasbuf[idx]);
            imgdata_set_pixels(x, y, 1, 1);
        }
    }
}

function draw_rect(x, y){
    imgdata_set_color(current_color);
    imgdata_set_pixels(x, y, brush_size, brush_size);
}

function buffer_set(x, y){
    for(let cury = y; cury < y + brush_size; ++cury){
        for(let curx = x; curx < x + brush_size; ++curx){
            canvasbuf[cury * canvas_size + curx] = current_color;
        }
    }
}

function buffer_clear(){
    for(let i = 0; i < canvas_size * canvas_size; ++i){
        canvasbuf[i] = CL_BLACK;
    }
}

function canvas_clear(){
    imgdata_set_color(CL_BLACK);
    imgdata_set_pixels(0, 0, canvas_size, canvas_size);
}

function get_mouse_pos(ev){
    let rect = ev.target.getBoundingClientRect();

    return [
        ev.clientX - rect.left,
        ev.clientY - rect.top
    ];
}

function interpolate_mouse_movement(mouse){
    let interpolated = [];
    let distx = mouse.lastx - mouse.x;
    let disty = mouse.lasty - mouse.y;
    let pydist = Math.sqrt(distx * distx +  disty * disty)|0;

    for(let i = 0; i < pydist; ++i){
        interpolated.push([
            mouse.lastx + ((i * (mouse.x - mouse.lastx)) / pydist)|0,
            mouse.lasty + ((i * (mouse.y - mouse.lasty)) / pydist)|0
        ]);
    }

    return interpolated;
}

function ev_mousemove(ev){
    let pos = get_mouse_pos(ev);

    mouse.lastx = mouse.x;
    mouse.lasty = mouse.y;
    mouse.x = pos[0];
    mouse.y = pos[1];
    mouse.interp = interpolate_mouse_movement(mouse);
}

function ev_mousedown(ev){
    if(ev.button == 0)
        mouse.down = 1;
}

function ev_mouseup(ev){
    if(ev.button == 0)
        mouse.down = 0;
}

function ev_mouseover(ev){
    show_cursor = 1;
}

function ev_mouseout(ev){
    show_cursor = 0;
}

function ev_clearbtn_click(ev){
    buffer_clear();
}

function generate_ev_btn_click(color){
    let display = document.querySelector(".colordisplay");

    return function(ev){
        current_color = color;
        display.innerText = "Selected: " + colornames[color];
    }
}

function ev_cltxt_keyup(ev){
    let preview = document.querySelector(".clpreview");

    preview.style.display = "inline";
    preview.style.background = "#" + ev.target.value;
}

function ev_clbtn_click(ev){
    let txt = document.querySelector("input.customcltxt");

    colors[CL_CUSTOM] = parseInt(txt.value, 16)|0;
    current_color = CL_CUSTOM;
}

function ev_bszbtn_click(ev){
    let txt = document.querySelector("input.custombsztxt");

    resize_brush(parseInt(txt.value, 10));
}

function ev_cszbtn_click(ev){
    let txt = document.querySelector("input.customcsztxt");

    resize_canvas(parseInt(txt.value, 10));
}

function ev_savebtn_click(ev){
    let url = canvas.toDataURL("image/png");
    let img = document.querySelector(".saveout");

    img.src = url;
}

function tick(){
    imgdata = ctx.createImageData(canvas_size, canvas_size);
    canvas_clear();
    draw_buffer();

    if(show_cursor)
        draw_rect(mouse.x, mouse.y);

    imgdata_draw();

    if(mouse.down){
        mouse.interp.forEach(function(coord){
            buffer_set(coord[0], coord[1]);
        });
    }

    requestAnimationFrame(tick);
}

function init(){
    let clearbtn = document.querySelector("input.clearbtn");
    let colorbtns = document.querySelector(".colorbtns");
    let customcltxt = document.querySelector("input.customcltxt");
    let customclbtn = document.querySelector("input.customclbtn");
    let custombszbtn = document.querySelector("input.custombszbtn");
    let customcszbtn = document.querySelector("input.customcszbtn");
    let savebtn = document.querySelector("input.savebtn");

    canvas.addEventListener("mousemove", ev_mousemove);
    canvas.addEventListener("mousedown", ev_mousedown);
    canvas.addEventListener("mouseup", ev_mouseup);
    canvas.addEventListener("mouseover", ev_mouseover);
    canvas.addEventListener("mouseout", ev_mouseout);
    clearbtn.addEventListener("click", ev_clearbtn_click);
    customcltxt.addEventListener("keyup", ev_cltxt_keyup);
    customclbtn.addEventListener("click", ev_clbtn_click);
    custombszbtn.addEventListener("click", ev_bszbtn_click);
    customcszbtn.addEventListener("click", ev_cszbtn_click);
    savebtn.addEventListener("click", ev_savebtn_click);

    for(let i = 0; i < CL_END; ++i){
        let btn = document.createElement("input");

        if(i % 8 == 0 && i > 0)
            colorbtns.appendChild(document.createElement("br"));

        colorbtns.appendChild(btn);
        btn.type = "button";
        btn.value = colornames[i];
        btn.addEventListener("click", generate_ev_btn_click(i));
    }

    resize_canvas(canvas_size);
}

init();
tick();
